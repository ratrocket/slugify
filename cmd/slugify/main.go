package main

import (
	"flag"
	"fmt"
	"os"

	"md0.org/slugify"
)

func main() {
	var (
		u = flag.Bool("u", false, "slugify with underscore")
	)
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [-u] <string-to-slugify>\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "  default is to slugify with '-'\n")
		flag.PrintDefaults()
		os.Exit(1)
	}
	flag.Parse()
	if flag.NArg() != 1 {
		flag.Usage()
	}
	f := slugify.WithDashes
	if *u {
		f = slugify.WithUnderscores
	}
	fmt.Println(f(flag.Arg(0)))
}
