package slugify

import (
	"bytes"
	"fmt"
	"log"
	"regexp"
	"strings"
	"unicode"

	"md0.org/asciifold"
)

// WithSep returns the slugified version of s, using sep as the slug
// separator.  It's written in a way that should make the rules of
// slugging easy to understand.
//
// NB: sep must be a single-character string, but since this function is
// private to the package, that should be easy to control.
func withSep(s string, sep string) string {
	if len(sep) != 1 {
		log.Fatalf("slugify.withSep called with too-long sep: %s\n", sep)
	}
	seps := regexp.MustCompile(fmt.Sprintf("%s+", sep))
	leadingSeps := regexp.MustCompile(fmt.Sprintf("^%s+", sep))
	trailingSeps := regexp.MustCompile(fmt.Sprintf("%s+$", sep))
	space := ' '
	ch := rune(sep[0])

	// Convert to ascii.
	s = asciifold.Fold(s)
	// Convert to lowercase.
	s = strings.ToLower(s)
	// Convert '&' to 'and' (order of these is important!).
	// Spaces are inserted so that "and" will never be squashed
	// against its neighbors.
	s = strings.Replace(s, "&amp;", " and ", -1)
	s = strings.Replace(s, "&", " and ", -1)
	// Replace single quote with nothing; this reads better in the
	// slugged output.
	s = strings.Replace(s, "'", "", -1)

	// Replace punctuation with space (which will become a `ch`.
	var buf bytes.Buffer
	for _, c := range s {
		if unicode.IsLetter(c) || unicode.IsNumber(c) || c == ch {
			buf.WriteRune(c)
		} else if unicode.IsSpace(c) {
			buf.WriteRune(space)
		} else if unicode.IsPunct(c) {
			buf.WriteRune(space)
		} else if unicode.IsSymbol(c) {
			buf.WriteRune(space)
		}
	}
	s = buf.String()

	// Replace space with sep.
	s = strings.Replace(s, " ", sep, -1)
	// Replace multiple seps with single sep.
	s = seps.ReplaceAllLiteralString(s, sep)
	// Remove leading seps.
	s = leadingSeps.ReplaceAllLiteralString(s, "")
	// Remove trailing seps.
	s = trailingSeps.ReplaceAllLiteralString(s, "")
	return s
}

func WithDashes(s string) string {
	return withSep(s, "-")
}

func WithUnderscores(s string) string {
	return withSep(s, "_")
}

func WithDashes0(s string) string {
	dashes := regexp.MustCompile(`-+`)
	leadingDashes := regexp.MustCompile(`^-+`)
	trailingDashes := regexp.MustCompile(`-+$`)
	space := ' '

	// Convert to ascii.
	s = asciifold.Fold(s)
	// Convert to lowercase.
	s = strings.ToLower(s)
	// Convert '&' to 'and' (order of these is important!).
	// Spaces are inserted so that "and" will never be squashed
	// against its neighbors.
	s = strings.Replace(s, "&amp;", " and ", -1)
	s = strings.Replace(s, "&", " and ", -1)
	// Replace single quote with nothing; this reads better in the
	// slugged output.
	s = strings.Replace(s, "'", "", -1)

	// Replace punctuation with space (which will become a '-').
	var buf bytes.Buffer
	for _, c := range s {
		if unicode.IsLetter(c) || unicode.IsNumber(c) || c == '-' {
			buf.WriteRune(c)
		} else if unicode.IsSpace(c) {
			buf.WriteRune(space)
		} else if unicode.IsPunct(c) {
			buf.WriteRune(space)
		} else if unicode.IsSymbol(c) {
			buf.WriteRune(space)
		}
	}
	s = buf.String()

	// Replace space with dash ('-').
	s = strings.Replace(s, " ", "-", -1)
	// Replace multiple dashes with single dash.
	s = dashes.ReplaceAllLiteralString(s, "-")
	// Remove leading dashes.
	s = leadingDashes.ReplaceAllLiteralString(s, "")
	// Remove trailing dashes.
	s = trailingDashes.ReplaceAllLiteralString(s, "")
	return s
}
